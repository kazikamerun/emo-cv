# pull official base image
FROM node:17.5

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH="/app/node_modules/.bin:$PATH"

# install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install -g npm@8.5.1
RUN npm ci --production

# add app
COPY . ./

EXPOSE 3000

# start app
CMD ["npm", "start"]